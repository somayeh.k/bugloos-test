import {Injectable} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {HttpClient} from '@angular/common/http';
import {User} from '../Model/user';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DataStorageService {
  constructor(private authService: AuthService,
              private http: HttpClient) {
  }

  // storeInformation(): object {
  //   const information = this.authService.getUserInformation();
  //   return this.http.put('https://taraneh-burger-145b4.firebaseio.com/information.json',
  //     information).subscribe(response => {
  //     console.log(response);
  //   });
  // }
}

