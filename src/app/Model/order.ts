export class Order {
  id: number;
  user: number;
  totalPrice: number;
  orderedAt: string;
}
