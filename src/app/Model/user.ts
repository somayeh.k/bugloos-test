export class User {
  constructor(
    public email: string,
    public id: string,
    private _token: string,
    private _tokenExpirationData: Date,
    public fullName?: string,
    public birthday?: string,
    public postalCode?: string,
    public address?: string,
    public password?: string,
  ) {
  }
}
