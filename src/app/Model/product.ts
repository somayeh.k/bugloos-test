export class Product {

  constructor(
    public id: number,
    public name: string,
    public description: string,
    public imagePath: { back: string; front: string },
    public price: string,
    public viewCount: number,
  ) {
  }
}

