import {Product} from './product';

export class OrderDetail {
  constructor(
    public product: Product,
    public price: string,
    public qty: number,
    public orderedAt: string,
  ) {
  }
}
