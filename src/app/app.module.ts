import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './views/header/header.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RegisterComponent} from './views/auth/register/register.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './views/auth/login/login.component';
import {AppRoutingModule} from './app-routing.module';
import { ProductListComponent } from './views/products/product-list/product-list.component';
import { ProductItemComponent } from './views/products/product-list/product-item/product-item.component';
import { HomeComponent } from './views/home/home.component';
import { ProductViewComponent } from './views/products/product-list/product-view/product-view.component';
import { ProductsComponent } from './views/products/products.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import { ProfileComponent } from './views/profile/profile.component';
import { ProfileUserComponent } from './views/profile/profile-user/profile-user.component';
import { BasketComponent } from './views/basket/basket.component';
import { BasketCheckoutComponent } from './views/basket/basket-checkout/basket-checkout.component';
import {CookieService} from 'ngx-cookie-service';
import { LogoutComponent } from './views/auth/logout/logout.component';
import {SlideshowModule} from 'ng-simple-slideshow';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterComponent,
    LoginComponent,
    ProductsComponent,
    ProductListComponent,
    ProductItemComponent,
    ProductViewComponent,
    HomeComponent,
    ProfileComponent,
    ProfileUserComponent,
    BasketComponent,
    BasketCheckoutComponent,
    LogoutComponent,
  ],
    imports: [
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        FormsModule,
        SlideshowModule,
    ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
