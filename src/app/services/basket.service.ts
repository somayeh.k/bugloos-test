import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {OrderDetail} from '../Model/order-detail';
import {Product} from '../Model/product';

@Injectable({
  providedIn: 'root'
})
export class BasketService {
  basket: OrderDetail[] = [];
  currentBasket = new BehaviorSubject<OrderDetail[]>(null);

  public addProduct(product: Product, qty: number = 1): void {
    const orderItem = new OrderDetail(
      product,
      product.price,
      qty,
      Date()
    );
    this.basket.push(orderItem);
    this.currentBasket.next(this.basket);
  }
}
