import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {User} from '../Model/user';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {AuthResponseDataInterface} from '../interfaces/auth-response-data.interface';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';


@Injectable({
  providedIn: 'root'
})

export class AuthService {

  profile = new BehaviorSubject<any>(null);
  user = new BehaviorSubject<User>(null);
  private registerUrl = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyABS9SoAvodGO1SGlgCjbwFtFyp52lnw5A';
  private loginUrl = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyABS9SoAvodGO1SGlgCjbwFtFyp52lnw5A';
  userInformation;
  fullName;
  email;
  birthday;
  postalCode;
  address;
  password;

  constructor(
    private http: HttpClient,
    private router: Router,
    private cookieService: CookieService
  ) {
    console.log(this.user);
  }

  getUserInformation(): BehaviorSubject<User> {
    return this.user;
  }

  signup(fullName: string, email: string, birthday: string, postalCode: string, address: string, password: string, confirmPassword: string):
    Observable<AuthResponseDataInterface> {
    return this.http.post<AuthResponseDataInterface>(this.registerUrl, {
        email,
        password,
        // fullName,
        // birthday,
        // postalCode,
        // address,
        returnSecureToken: true
      }
    ).pipe(catchError(this.handleError),
      tap(resData => {
        const username = this.correctUserName(email, true);
        this.http.post('https://taraneh-burger-145b4.firebaseio.com/Users/' + username + '.json',
          {
            fullName,
            email,
            birthday,
            postalCode,
            address,
            password
          }
        ).subscribe((s) => {
          console.log(s);
        });
        // this.handleAuthentication(
        //   resData.email,
        //   resData.localId,
        //   resData.idToken,
        //   +resData.expiresIn
        // );
      }));
  }

  fetchInformation(): Observable<any> {
    return this.http.get('https://taraneh-burger-145b4.firebaseio.com/User.json');
  }

  private handleAuthentication(email: string, userId: string, token: string, expiresIn: number): void {
    const expirationDate = new Date(
      new Date().getTime() + expiresIn * 1000
    );
    const user = new User(email, userId, token, expirationDate);
    this.user.next(user);
    this.cookieService.set('token', token);
    this.cookieService.set('user', JSON.stringify(user));
  }

  private handleError(errorRes: HttpErrorResponse): Observable<never> {
    console.log(errorRes);
    let errorMessage = 'An unknown error occurred!';
    if (!errorRes.error || !errorRes.error.error) {
      return throwError(errorMessage);
    }
    switch (errorRes.error.error.message) {
      case 'EMAIL_EXISTS':
        errorMessage = 'This email exists already!';
        break;
      case 'EMAIL_NOT_FOUND':
        errorMessage = 'This email does not exit.';
        break;
      case 'INVALID_PASSWORD':
        errorMessage = 'This password in incorrect';
    }
    return throwError(errorMessage);
  }

  correctUserName(originalEmail: string, Replace: boolean = true) {
    let username = originalEmail.slice(0, originalEmail.indexOf('@'));
    if (Replace) {
      username = username.replace('.', '_');
    }
    return username;
  }


  login(email: string, password: string): Observable<AuthResponseDataInterface> {
    return this.http.post<AuthResponseDataInterface>(this.loginUrl, {
        email,
        password
      }
    ).pipe(catchError(this.handleError),
      tap(resData => {
        this.handleAuthentication(
          resData.email,
          resData.localId,
          resData.idToken,
          +resData.expiresIn
        );
      }));
  }

  logout(): void {
    this.user.next(null);
    this.cookieService.delete('user');
    this.cookieService.delete('token');
    this.cookieService.deleteAll();
    this.router.navigate(['/login']);
  }
}
