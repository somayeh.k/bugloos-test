import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {Product} from '../Model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  product = [];
  sortProduct;
  popularProducts;
  cheapestProduct;
  newAddedProducts;
  private products: Product[] = [
    new Product(
      1,
      'Women\'s Blouse',
      'Solids: 100% Cotton; Heathers: 60% Cotton, 40% Polyester\n' +
      'Imported\n' +
      'Machine Wash\n' +
      'This 2-pack of classic, lightweight tees feature all-cotton construction and a tag-free crew neckline for everyday comfort\n' +
      'These classic tees feature all-cotton construction and a tag-free crew neckline for comfort\n' +
      'Everyday made better: we listen to customer feedback and fine-tune every detail to ensure quality, fit, and comfort', {
        front: 'assets/images/products/img-1.jpg',
        back: 'assets/images/products/img-2.jpg'
      },
      '1510',
      120
    ),
    new Product(
      2,
      'Men\'s T-shirt',
      'Do Not Bleach\n' +
      '【Designed for All-Season】Great Performance in warm or cool weather. Sweat and Moisture can be pulled away from skin very quickly; Enable sweat easy for air-dry to prevent clothes sticking to the body.\n' +
      '【Functional Fabric】 4-Way Stretch / Moisture Wicking / Two-Way Air Circulation / Quick-Dry\n' +
      '【UV Sun Protection】 Rating UPF 50+ protects your skin from the sun\'s harmful UV rays.\n' +
      '【Comfortable】 Non abrasion spandex fabric with excellent elasticity and durability for maximum mobility.\n' +
      '【Multi-Versatile】 Perfect for daily gym, fitness, jogging, running, football, soccer, yoga, cycle, bjj and other active workouts.', {
        front: 'assets/images/products/img-3.jpg',
        back: 'assets/images/products/img-4.jpg'
      },
      '5020',
      200
    ),
    new Product(
      3,
      'Woman\'s Colorful shirt',
      'Solids: 100% Cotton; Sport Grey: 90% Cotton, 10% Polyester\n' +
      'Imported\n' +
      'Machine Wash\n' +
      'Moisture wicking keeps you cool and dry\n' +
      'Taped neck and shoulders for durability; Tubular rib collar for better stretch and recovery\n' +
      'Feels soft to the touch; Tag free; Lays flat\n' +
      '5 pack = S, M, XL; 4 pack = 2X (Colors may vary)', {
        front: 'assets/images/products/img-5.jpg',
        back: 'assets/images/products/img-6.jpg'
      },
      '2010',
      250
    ),
    new Product(
      4,
      'Men\'s Plain T-shirt',
      'a good laptop',
      {
        front: 'assets/images/products/img-7.jpg',
        back: 'assets/images/products/img-8.jpg'
      },
      '4000',
      180
    ),
    new Product(
      5,
      'Woman\'s Shirt',
      'Solids: 100% Cotton; Sport Grey And Antique Heather: 90% Cotton, 10% Polyester; Safety Colors And Heather: 50% Cotton, 50% Polyester\n' +
      'Imported\n' +
      'Pull On closure\n' +
      'Machine Wash\n' +
      'Longer dropped shoulder, straighter armhole, and wider, shorter sleeves\n' +
      'Double-stitching at the hems to make it built to last\n' +
      'Thick and hefty fabric', {
        front: 'assets/images/products/img-9.jpg',
        back: 'assets/images/products/img-10.jpg'
      },
      '1720',
      452
    ),
    new Product(
      6,
      'Man\'s Coat',
      'Solids: 100% Cotton; Sport Grey And Antique Heather: 90% Cotton, 10% Polyester; Safety Colors And Heather: 50% Cotton, 50% Polyester\n' +
      'Imported\n' +
      'Pull On closure\n' +
      'Machine Wash\n' +
      'Longer dropped shoulder, straighter armhole, and wider, shorter sleeves\n' +
      'Double-stitching at the hems to make it built to last\n' +
      'Thick and hefty fabric', {
        front: 'assets/images/products/img-11.jpg',
        back: 'assets/images/products/img-12.jpg'
      },
      '1000',
      321
    ),
    new Product(
      7,
      'Woman\'s White Shirt',
      'Solids: 100% Cotton; Sport Grey And Antique Heather: 90% Cotton, 10% Polyester; Safety Colors And Heather: 50% Cotton, 50% Polyester\n' +
      'Imported\n' +
      'Pull On closure\n' +
      'Machine Wash\n' +
      'Longer dropped shoulder, straighter armhole, and wider, shorter sleeves\n' +
      'Double-stitching at the hems to make it built to last\n' +
      'Thick and hefty fabric', {
        front: 'assets/images/products/img-13.jpg',
        back: 'assets/images/products/img-14.jpg'
      },
      '2072',
      290
    ),
    new Product(
      8,
      'Man\'s Jacket',
      'Solids: 100% Cotton; Sport Grey And Antique Heather: 90% Cotton, 10% Polyester; Safety Colors And Heather: 50% Cotton, 50% Polyester\n' +
      'Imported\n' +
      'Pull On closure\n' +
      'Machine Wash\n' +
      'Longer dropped shoulder, straighter armhole, and wider, shorter sleeves\n' +
      'Double-stitching at the hems to make it built to last\n' +
      'Thick and hefty fabric', {
        front: 'assets/images/products/img-15.jpg',
        back: 'assets/images/products/img-16.jpg'
      },
      '3210',
      178
    ),
    new Product(
      9,
      'Woman\'s Skirt',
      'Solids: 100% Cotton; Sport Grey And Antique Heather: 90% Cotton, 10% Polyester; Safety Colors And Heather: 50% Cotton, 50% Polyester\n' +
      'Imported\n' +
      'Pull On closure\n' +
      'Machine Wash\n' +
      'Longer dropped shoulder, straighter armhole, and wider, shorter sleeves\n' +
      'Double-stitching at the hems to make it built to last\n' +
      'Thick and hefty fabric', {
        front: 'assets/images/products/img-17.jpg',
        back: 'assets/images/products/img-18.jpg'
      },
      '5050',
      202
    ),
    new Product(
      10,
      'Man\'s Blue Shirt',
      'Solids: 100% Cotton; Sport Grey And Antique Heather: 90% Cotton, 10% Polyester; Safety Colors And Heather: 50% Cotton, 50% Polyester\n' +
      'Imported\n' +
      'Pull On closure\n' +
      'Machine Wash\n' +
      'Longer dropped shoulder, straighter armhole, and wider, shorter sleeves\n' +
      'Double-stitching at the hems to make it built to last\n' +
      'Thick and hefty fabric', {
        front: 'assets/images/products/img-19.jpg',
        back: 'assets/images/products/img-20.jpg'
      },
      '1500',
      152
    ),
    new Product(
      11,
      'Man\'s Shirt',
      'Solids: 100% Cotton; Sport Grey And Antique Heather: 90% Cotton, 10% Polyester; Safety Colors And Heather: 50% Cotton, 50% Polyester\n' +
      'Imported\n' +
      'Pull On closure\n' +
      'Machine Wash\n' +
      'Longer dropped shoulder, straighter armhole, and wider, shorter sleeves\n' +
      'Double-stitching at the hems to make it built to last\n' +
      'Thick and hefty fabric', {
        front: 'assets/images/products/img-21.jpg',
        back: 'assets/images/products/img-22.jpg'
      },
      '1050',
      320
    ),
    new Product(
      12,
      'Woman\'s pens',
      'Solids: 100% Cotton; Sport Grey And Antique Heather: 90% Cotton, 10% Polyester; Safety Colors And Heather: 50% Cotton, 50% Polyester\n' +
      'Imported\n' +
      'Pull On closure\n' +
      'Machine Wash\n' +
      'Longer dropped shoulder, straighter armhole, and wider, shorter sleeves\n' +
      'Double-stitching at the hems to make it built to last\n' +
      'Thick and hefty fabric', {
        front: 'assets/images/products/img-23.jpg',
        back: 'assets/images/products/img-24.jpg'
      },
      '3050',
      450
    ),
  ];


  getProducts(): Product[] {
    return this.products;
  }

  getProduct(id: number): Product {
    return this.products.find(p => Number(p.id) === id);
  }

  getPopularProducts(): void {
    this.sortProduct = _.sortBy(this.products, ['viewCount']);
    this.popularProducts = this.sortProduct.slice(Math.max(this.sortProduct.length - 3, 0));
    return this.popularProducts;
  }

  getCheapestProduct(): void {
    this.cheapestProduct = _.sortBy(this.products, ['price']);
    return this.cheapestProduct;
  }
}
