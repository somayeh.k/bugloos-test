import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {DataStorageService} from '../../../shared/data-storage.service';
import {HttpClient} from '@angular/common/http';
import {User} from '../../../Model/user';

@Component({
  selector: 'app-profile-user',
  templateUrl: './profile-user.component.html',
  styleUrls: ['./profile-user.component.scss']
})
export class ProfileUserComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  profile: any;
  userEditProfile: FormGroup;
  userInformation;

  constructor(private authService: AuthService,
              private http: HttpClient) {
  }

  ngOnInit(): void {
    // this.authService.profile.subscribe(
    //   (profile) => {
    //     this.profile = profile;
    //     this.form.setValue({
    //       fullName: this.profile.fullName,
    //       email: this.profile.email,
    //       birthday: this.profile.birthday,
    //       postalCode: this.profile.postalCode,
    //       address: this.profile.address,
    //       password: this.profile.password,
    //     });
    //   }
    // );
    // this.userInformation = this.authService.fetchInformation().subscribe(resData => {
    //   console.log(resData);
    // this.userEditProfile = new FormGroup({
    //   fullName: new FormControl(this.userInformation.fullName),
    //   email: new FormControl(this.userInformation.email),
    //   birthday: new FormControl(this.userInformation.birthday),
    //   postalCode: new FormControl(this.userInformation.postalCode),
    //   address: new FormControl(this.userInformation.address),
    //   password: new FormControl(this.userInformation.password)
    // });
    // this.userEditProfile.setValue({
    //   fullName: resData.fullName,
    //   email: resData.email,
    //   birthday: resData.birthday,
    //   postalCode: resData.postalCode,
    //   address: resData.address,
    //   password: resData.password,
    // });
  }

}

// this.userEditProfile = new FormGroup({
//   fullName: new FormControl(''),
//   email: new FormControl(null),
//   birthday: new FormControl(null),
//   postalCode: new FormControl(null),
//   address: new FormControl(null),
//   password: new FormControl(null),
// });
// this.userInformation = this.authService.fetchInformation().subscribe(
//     (user: User[]) => {
//       this.userInformation = user;
//       this.profileEditForm.setValue({
//         fullName: this.user.fullName,
//       });
//       console.log(this.userInformation);
//     }
//   );
// }

// onSubmitUserEditForm(): void {
//   if (!this.userEditForm.valid) {
//     return;
//   }
// }

// onResetForm(): void {
//   this.userEditForm.reset();
// }

