import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
}
