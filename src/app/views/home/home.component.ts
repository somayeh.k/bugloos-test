import {Component, Input, OnInit} from '@angular/core';
import {ProductService} from '../../services/product.service';
import {Product} from '../../Model/product';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @Input() products: Product[];
  popularProducts;
  images = [];
  image;
  newAddedProducts;
  popularProduct = [];

  constructor(private productService: ProductService) {
  }

  ngOnInit(): void {
    this.products = this.productService.getProducts();
    this.popularProducts = this.productService.getPopularProducts();
    this.popularProducts.forEach(product => {
      this.popularProduct.push(product);
      // return this.popularProduct =  product.imagePath.back;
    });
    console.log(this.popularProduct);
    this.newAddedProducts = this.products.slice(Math.max(this.products.length - 4, 0));
  }
}
