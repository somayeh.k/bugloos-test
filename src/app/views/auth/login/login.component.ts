import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  signInForm: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient
  ) {
  }


  ngOnInit(): void {
    this.signInForm = new FormGroup({
      email: new FormControl(null, [
          Validators.required,
          Validators.email
        ]
      ),
      password: new FormControl(null, Validators.required),
    });
  }


  onSubmit(): void {
    console.log(this.signInForm);
    if (!this.signInForm.valid) {
      return;
    }
    const email = this.signInForm.value.email;
    const password = this.signInForm.value.password;
    this.authService.login(
      email,
      password
    ).subscribe(resData => {
        const username = this.authService.correctUserName(email, true);
        this.http.get('https://taraneh-burger-145b4.firebaseio.com/Users/' + username + '.json')
          .subscribe((responseData) => {
            const postsArray = [];
            for (const key in responseData) {
              if (responseData.hasOwnProperty(key)) {
                postsArray.push({...responseData[key], id: key});
              }
            }

            this.authService.profile.next(postsArray[0]);
            console.log(postsArray[0]);
          });

        //
        // console.log(resData);
        this.router.navigate(['/profile'], {relativeTo: this.route});
      },
      error => {
        console.log(error);
      });
    this.signInForm.reset();
  }


}
