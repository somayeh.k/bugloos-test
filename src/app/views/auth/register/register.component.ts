import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DataStorageService} from '../../../shared/data-storage.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  signUpForm: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private dataStorageService: DataStorageService
  ) {
  }

  ngOnInit(): void {
    this.signUpForm = new FormGroup({
      fullName: new FormControl(null, [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(100)
        ]
      ),
      email: new FormControl(null, [
          Validators.required,
          Validators.email,
          Validators.maxLength(150)
        ]
      ),
      birthday: new FormControl(null, Validators.required),
      address: new FormControl(null, [
          Validators.required,
          Validators.minLength(20),
          Validators.maxLength(100)
        ]
      ),
      postalCode: new FormControl(null, Validators.minLength(10),
      ),
      // passwords: new FormGroup({
      password: new FormControl(null, Validators.required),
      confirmPassword: new FormControl(null, Validators.required)
      // })
    });
  }


  onSubmit(): void {
    if (!this.signUpForm.valid) {
      return;
    }
    const fullName = this.signUpForm.value.fullName;
    const email = this.signUpForm.value.email;
    const birthday = this.signUpForm.value.birthday;
    const postalCode = this.signUpForm.value.postalCode;
    const address = this.signUpForm.value.address;
    const password = this.signUpForm.value.password;
    const confirmPassword = this.signUpForm.value.confirmPassword;

    this.authService.signup(fullName, email, birthday, postalCode, address, password, confirmPassword).subscribe(resData => {
        this.router.navigate(['/login'], {relativeTo: this.route});
      },
      error => {
        console.log(error);
      });
    this.signUpForm.reset();
  }

}
