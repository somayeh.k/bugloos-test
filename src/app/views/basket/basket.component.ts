import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basket',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent {
}
