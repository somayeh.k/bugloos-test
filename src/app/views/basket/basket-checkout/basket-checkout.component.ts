import {Component, Input, OnInit} from '@angular/core';
import {BasketService} from '../../../services/basket.service';
import {OrderDetail} from '../../../Model/order-detail';

@Component({
  selector: 'app-basket-checkout',
  templateUrl: './basket-checkout.component.html',
  styleUrls: ['./basket-checkout.component.scss']
})
export class BasketCheckoutComponent implements OnInit {
   orderDetails: OrderDetail[];
   totalPrice = 0;
  constructor(private basketService: BasketService) {
  }

  ngOnInit(): void {
    this.basketService.currentBasket.subscribe(
      (orderDetails: OrderDetail[]) => {
        this.orderDetails = orderDetails;
      }
    );
    this.orderDetails.forEach(orderDetail => {
      this.totalPrice += Number(orderDetail.price) * Number(orderDetail.qty);
    });
  }

}
