import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../../Model/product';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../services/product.service';
import * as _ from 'lodash';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  products: Product[];
  index: number;
  product;
  cheapestProduct;
  search;
  a;

  constructor(private productService: ProductService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.products = this.productService.getProducts();
  }

  onCheapestProduct(): void {
    this.cheapestProduct = this.productService.getCheapestProduct();
    this.products = this.cheapestProduct;
  }

  onNormalView(): void {
    this.products = this.productService.getProducts();
  }

  onSearchProduct(event: Event): void {
    this.search = (event.target as HTMLInputElement).value;
    console.log(this.search);
    const result = this.productService.getProducts().filter(product => {
      return product.name.includes(this.search);
    });
    this.products = result;
  }
}
