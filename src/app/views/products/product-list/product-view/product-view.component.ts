import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Product} from '../../../../Model/product';
import {BasketService} from '../../../../services/basket.service';
import {AuthService} from '../../../../services/auth.service';
import {ProductService} from '../../../../services/product.service';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit {
  public product: Product;
  public qty;
  public alreadyAdded = false;
  public isLoggedIn = false;
  products;
  productsSimilar;
  constructor(
    private route: ActivatedRoute,
    private basket: BasketService,
    private authService: AuthService,
    private productService: ProductService,
  ) {
    this.authService.user.subscribe(user => {
      this.isLoggedIn = !!user;
    });
  }

  ngOnInit(): void {
    this.route.data.subscribe(resolver => {
      this.product = resolver.product;
    });
    this.products = this.productService.getProducts().slice(4, 7);
    this.productsSimilar = this.productService.getProducts().slice(9, 12);
  }

  addToCart(): void {
    if (!this.isLoggedIn) {
      return;
    }
    this.basket.addProduct(this.product, this.qty);
    this.alreadyAdded = true;
  }
}
