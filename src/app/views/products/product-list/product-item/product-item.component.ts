import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {ProductService} from '../../../../services/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from '../../../../Model/product';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss'],
})
export class ProductItemComponent implements OnInit {
  @Input() product: Product;
  // id: number;

  constructor(private productService: ProductService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void { }

  onProduct(): void {
    this.router.navigate(['/products', this.product.id]);
  }

}
