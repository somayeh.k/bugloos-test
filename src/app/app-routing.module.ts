import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegisterComponent} from './views/auth/register/register.component';
import {LoginComponent} from './views/auth/login/login.component';
import {HomeComponent} from './views/home/home.component';
import {ProductListComponent} from './views/products/product-list/product-list.component';
import {ProductViewComponent} from './views/products/product-list/product-view/product-view.component';
import {ProductsComponent} from './views/products/products.component';
import {ProductResolver} from './resolvers/product.resolver';
import {ProfileComponent} from './views/profile/profile.component';
import {ProfileUserComponent} from './views/profile/profile-user/profile-user.component';
import {BasketComponent} from './views/basket/basket.component';
import {BasketCheckoutComponent} from './views/basket/basket-checkout/basket-checkout.component';
import {LogoutComponent} from './views/auth/logout/logout.component';
import {AuthGuard} from './guards/auth.guard';

const appRoutes: Routes = [
  // {path: '', component: ProfileUserComponent}
  {path: 'home', component: HomeComponent},
  {path: '', redirectTo: '/products', pathMatch: 'full'},
  {
    path: 'products',
    component: ProductsComponent,
    children: [
      {
        path: '',
        component: ProductListComponent
      },
      {
        path: ':id',
        component: ProductViewComponent,
        resolve: {
          product: ProductResolver
        }
      }
    ]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: ProfileUserComponent
      },
    ]
  },
  {
    path: 'basket',
    component: BasketComponent,
    children: [
      {
        path: '',
        component: BasketCheckoutComponent
      },
    ]
  },
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'logout', component: LogoutComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
